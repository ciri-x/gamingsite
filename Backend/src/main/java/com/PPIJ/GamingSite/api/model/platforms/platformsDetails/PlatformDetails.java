package com.PPIJ.GamingSite.api.model.platforms.platformsDetails;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "slug",
        "games_count",
        "image_background",
        "description",
        "image",
        "year_start",
        "year_end"
})
public class PlatformDetails {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("slug")
    private String slug;
    @JsonProperty("games_count")
    private Integer gamesCount;
    @JsonProperty("image_background")
    private String imageBackground;
    @JsonProperty("description")
    private String description;
    @JsonProperty("image")
    private String image;
    @JsonProperty("year_start")
    private Integer yearStart;
    @JsonProperty("year_end")
    private Integer yearEnd;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("slug")
    public String getSlug() {
        return slug;
    }

    @JsonProperty("slug")
    public void setSlug(String slug) {
        this.slug = slug;
    }

    @JsonProperty("games_count")
    public Integer getGamesCount() {
        return gamesCount;
    }

    @JsonProperty("games_count")
    public void setGamesCount(Integer gamesCount) {
        this.gamesCount = gamesCount;
    }

    @JsonProperty("image_background")
    public String getImageBackground() {
        return imageBackground;
    }

    @JsonProperty("image_background")
    public void setImageBackground(String imageBackground) {
        this.imageBackground = imageBackground;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("image")
    public String getImage() {
        return image;
    }

    @JsonProperty("image")
    public void setImage(String image) {
        this.image = image;
    }

    @JsonProperty("year_start")
    public Integer getYearStart() {
        return yearStart;
    }

    @JsonProperty("year_start")
    public void setYearStart(Integer yearStart) {
        this.yearStart = yearStart;
    }

    @JsonProperty("year_end")
    public Integer getYearEnd() {
        return yearEnd;
    }

    @JsonProperty("year_end")
    public void setYearEnd(Integer yearEnd) {
        this.yearEnd = yearEnd;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

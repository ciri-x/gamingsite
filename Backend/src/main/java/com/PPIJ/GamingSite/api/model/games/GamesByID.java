package com.PPIJ.GamingSite.api.model.games;

import com.PPIJ.GamingSite.api.model.Wishlist;
import com.PPIJ.GamingSite.api.model.enums.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class GamesByID{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int ID;

    private Integer gameID;
    private String gameTitle;
    private Double gameRating;

    public GamesByID(Integer gameID, String gameTitle, Double gameRating) {
        this.gameID= gameID;
        this.gameTitle = gameTitle;
        this.gameRating = gameRating;
    }

}


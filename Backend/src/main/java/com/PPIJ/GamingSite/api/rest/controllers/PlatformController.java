package com.PPIJ.GamingSite.api.rest.controllers;

import com.PPIJ.GamingSite.api.model.games.ListOfGames;
import com.PPIJ.GamingSite.api.model.platforms.platformsDetails.PlatformDetails;
import com.PPIJ.GamingSite.api.model.platforms.Platforms;
import com.PPIJ.GamingSite.api.model.platforms.Result;
import com.PPIJ.GamingSite.api.rest.client.AllGamesRestClient;
import com.PPIJ.GamingSite.api.rest.client.AllPlatformsClient;
import com.PPIJ.GamingSite.api.rest.client.PlatformDetailClient;
import com.PPIJ.GamingSite.api.rest.client.PlatformDetailClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/platforms")
public class PlatformController {

    @Autowired
    PlatformDetailClient platformDetailClient;

    @Autowired
    AllPlatformsClient allPlatformsClient;

    @GetMapping("/{id}")
    public PlatformDetails getPlatformDetails(@PathVariable int id) {
        return platformDetailClient.getPlatformDetails(id);
    }

    @GetMapping("")
    public List<Result> getFirstFewPlatforms(){
        return allPlatformsClient.getFirstFewPlatforms();
    }

    @GetMapping("/page={page}")
    public Platforms getAllPlatforms(@PathVariable String page){
        return allPlatformsClient.getAllPlatforms(page);
    }


}

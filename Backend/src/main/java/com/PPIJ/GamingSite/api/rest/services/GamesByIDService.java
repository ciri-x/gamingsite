package com.PPIJ.GamingSite.api.rest.services;

import com.PPIJ.GamingSite.api.dao.GamesByIDRepository;
import com.PPIJ.GamingSite.api.model.games.GamesByID;
import com.PPIJ.GamingSite.api.rest.client.AllGamesRestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GamesByIDService {
    @Autowired
    private GamesByIDRepository gamesByIDRepository;

    @Autowired
    private AllGamesRestClient allGamesRestClient;

    public List<GamesByID> getGamesByID() {
        return gamesByIDRepository.findAll();
    }

    public List<GamesByID> getGamesSortedByRating(){return gamesByIDRepository.findByOrderByGameRatingDesc();}

    public GamesByID getGamesByIDByID(int id) {
        return gamesByIDRepository.findByGameID(id);
    }

    public GamesByID getGamesByIDByTitle(String title) {
        return gamesByIDRepository.findByGameTitle(title);
    }

    public Integer saveGame(Integer firstPage, Integer lastPage) {

        List<GamesByID> list = allGamesRestClient.getAllGamesByID(firstPage, lastPage);

        for(GamesByID game : list){
            GamesByID newGame = new GamesByID(game.getGameID(), game.getGameTitle(), game.getGameRating());
            gamesByIDRepository.save(newGame);
        }

        return getGamesByID().size();
    }
}

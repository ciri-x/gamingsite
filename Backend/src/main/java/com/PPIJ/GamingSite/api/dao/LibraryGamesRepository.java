package com.PPIJ.GamingSite.api.dao;

import com.PPIJ.GamingSite.api.model.Library;
import com.PPIJ.GamingSite.api.model.LibraryGames;
import com.PPIJ.GamingSite.api.model.WishlistGames;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface LibraryGamesRepository extends JpaRepository<LibraryGames, Integer> {
    List<LibraryGames> findByLibraryID(int libraryID);
    Optional<LibraryGames> findByGameIDAndLibraryID(int gameID, int libraryID);
}

package com.PPIJ.GamingSite.api.rest.controllers;

import com.PPIJ.GamingSite.api.model.games.GamesByID;
import com.PPIJ.GamingSite.api.model.games.ListOfGames;
import com.PPIJ.GamingSite.api.model.games.Result;
import com.PPIJ.GamingSite.api.model.games.gameDetails.GameDetails;

import com.PPIJ.GamingSite.api.rest.client.AllGamesRestClient;

import com.PPIJ.GamingSite.api.rest.client.GameDetailClient;

import com.PPIJ.GamingSite.api.rest.services.GamesByIDService;
import com.PPIJ.GamingSite.util.AuthorizationUtil;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/games")
public class GamesController {

    @Autowired
    AllGamesRestClient allGamesRestClient;
    @Autowired
    GameDetailClient gameDetailClient;
    @Autowired
    GamesByIDService gamesByIDService;


    @Autowired
    private AuthorizationUtil authorizationUtil;

    @GetMapping("/{id}")
    public GameDetails getGameDetails(@PathVariable int id) {
        return gameDetailClient.getGameDetails(id);
    }
    @GetMapping("")
    public ListOfGames getAllGames(String page) {

        return allGamesRestClient.getAllGames(page);
    }

   @GetMapping("/byID")
    public List<GamesByID> getAllGamesByID() {
        return gamesByIDService.getGamesByID();
    }

    @GetMapping("/loadGamesFromApi")
    public Integer loadGamesFromAPI(Integer firstPage, Integer lastPage) {
        return gamesByIDService.saveGame(firstPage, lastPage);
    }

    @GetMapping("/gamesSortedByRating")
    public List<GamesByID> getSortedByRating(){
        return gamesByIDService.getGamesSortedByRating();
    }



}

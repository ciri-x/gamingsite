
package com.PPIJ.GamingSite.api.model.games;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "count",
    "next",
    "previous",
    "results",
    "seo_title",
    "seo_description",
    "seo_keywords",
    "seo_h1",
    "noindex",
    "nofollow",
    "description",
    "filters",
    "nofollow_collections"
})
public class ListOfGames {

    @JsonProperty("count")
    private Integer count;
    @JsonProperty("next")
    private String next;
    @JsonProperty("previous")
    private Object previous;
    @JsonProperty("results")
    private List<Result> results = null;
    @JsonProperty("seo_title")
    private String seoTitle;
    @JsonProperty("seo_description")
    private String seoDescription;
    @JsonProperty("seo_keywords")
    private String seoKeywords;
    @JsonProperty("seo_h1")
    private String seoH1;
    @JsonProperty("noindex")
    private Boolean noindex;
    @JsonProperty("nofollow")
    private Boolean nofollow;
    @JsonProperty("description")
    private String description;
    @JsonProperty("filters")
    private Filters filters;
    @JsonProperty("nofollow_collections")
    private List<String> nofollowCollections = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("count")
    public Integer getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(Integer count) {
        this.count = count;
    }

    @JsonProperty("next")
    public String getNext() {
        return next;
    }

    @JsonProperty("next")
    public void setNext(String next) {
        this.next = next;
    }

    @JsonProperty("previous")
    public Object getPrevious() {
        return previous;
    }

    @JsonProperty("previous")
    public void setPrevious(Object previous) {
        this.previous = previous;
    }

    @JsonProperty("results")
    public List<Result> getResults() {
        return results;
    }

    @JsonProperty("results")
    public void setResults(List<Result> results) {
        this.results = results;
    }

    @JsonProperty("seo_title")
    public String getSeoTitle() {
        return seoTitle;
    }

    @JsonProperty("seo_title")
    public void setSeoTitle(String seoTitle) {
        this.seoTitle = seoTitle;
    }

    @JsonProperty("seo_description")
    public String getSeoDescription() {
        return seoDescription;
    }

    @JsonProperty("seo_description")
    public void setSeoDescription(String seoDescription) {
        this.seoDescription = seoDescription;
    }

    @JsonProperty("seo_keywords")
    public String getSeoKeywords() {
        return seoKeywords;
    }

    @JsonProperty("seo_keywords")
    public void setSeoKeywords(String seoKeywords) {
        this.seoKeywords = seoKeywords;
    }

    @JsonProperty("seo_h1")
    public String getSeoH1() {
        return seoH1;
    }

    @JsonProperty("seo_h1")
    public void setSeoH1(String seoH1) {
        this.seoH1 = seoH1;
    }

    @JsonProperty("noindex")
    public Boolean getNoindex() {
        return noindex;
    }

    @JsonProperty("noindex")
    public void setNoindex(Boolean noindex) {
        this.noindex = noindex;
    }

    @JsonProperty("nofollow")
    public Boolean getNofollow() {
        return nofollow;
    }

    @JsonProperty("nofollow")
    public void setNofollow(Boolean nofollow) {
        this.nofollow = nofollow;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("filters")
    public Filters getFilters() {
        return filters;
    }

    @JsonProperty("filters")
    public void setFilters(Filters filters) {
        this.filters = filters;
    }

    @JsonProperty("nofollow_collections")
    public List<String> getNofollowCollections() {
        return nofollowCollections;
    }

    @JsonProperty("nofollow_collections")
    public void setNofollowCollections(List<String> nofollowCollections) {
        this.nofollowCollections = nofollowCollections;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}


package com.PPIJ.GamingSite.api.model.recommended;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "platform",
    "released_at",
    "requirements_en",
    "requirements_ru"
})
public class Platform {

    @JsonProperty("platform")
    private Platform_ platform;
    @JsonProperty("released_at")
    private Object releasedAt;
    @JsonProperty("requirements_en")
    private RequirementsEn requirementsEn;
    @JsonProperty("requirements_ru")
    private Object requirementsRu;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("platform")
    public Platform_ getPlatform() {
        return platform;
    }

    @JsonProperty("platform")
    public void setPlatform(Platform_ platform) {
        this.platform = platform;
    }

    @JsonProperty("released_at")
    public Object getReleasedAt() {
        return releasedAt;
    }

    @JsonProperty("released_at")
    public void setReleasedAt(Object releasedAt) {
        this.releasedAt = releasedAt;
    }

    @JsonProperty("requirements_en")
    public RequirementsEn getRequirementsEn() {
        return requirementsEn;
    }

    @JsonProperty("requirements_en")
    public void setRequirementsEn(RequirementsEn requirementsEn) {
        this.requirementsEn = requirementsEn;
    }

    @JsonProperty("requirements_ru")
    public Object getRequirementsRu() {
        return requirementsRu;
    }

    @JsonProperty("requirements_ru")
    public void setRequirementsRu(Object requirementsRu) {
        this.requirementsRu = requirementsRu;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

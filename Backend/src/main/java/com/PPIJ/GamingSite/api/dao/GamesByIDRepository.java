package com.PPIJ.GamingSite.api.dao;

import com.PPIJ.GamingSite.api.model.games.GamesByID;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GamesByIDRepository extends JpaRepository<GamesByID, Integer>{
    GamesByID findByGameID(int gameID);
    GamesByID findByGameTitle(String gameTitle);
    GamesByID findByGameRating(Double gameRating);
    List<GamesByID> findByOrderByGameRatingDesc();
}

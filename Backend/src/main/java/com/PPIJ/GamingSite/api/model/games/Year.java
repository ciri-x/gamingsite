
package com.PPIJ.GamingSite.api.model.games;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "from",
    "to",
    "filter",
    "decade",
    "years",
    "nofollow",
    "count"
})
public class Year {

    @JsonProperty("from")
    private Integer from;
    @JsonProperty("to")
    private Integer to;
    @JsonProperty("filter")
    private String filter;
    @JsonProperty("decade")
    private Integer decade;
    @JsonProperty("years")
    private List<Year_> years = null;
    @JsonProperty("nofollow")
    private Boolean nofollow;
    @JsonProperty("count")
    private Integer count;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("from")
    public Integer getFrom() {
        return from;
    }

    @JsonProperty("from")
    public void setFrom(Integer from) {
        this.from = from;
    }

    @JsonProperty("to")
    public Integer getTo() {
        return to;
    }

    @JsonProperty("to")
    public void setTo(Integer to) {
        this.to = to;
    }

    @JsonProperty("filter")
    public String getFilter() {
        return filter;
    }

    @JsonProperty("filter")
    public void setFilter(String filter) {
        this.filter = filter;
    }

    @JsonProperty("decade")
    public Integer getDecade() {
        return decade;
    }

    @JsonProperty("decade")
    public void setDecade(Integer decade) {
        this.decade = decade;
    }

    @JsonProperty("years")
    public List<Year_> getYears() {
        return years;
    }

    @JsonProperty("years")
    public void setYears(List<Year_> years) {
        this.years = years;
    }

    @JsonProperty("nofollow")
    public Boolean getNofollow() {
        return nofollow;
    }

    @JsonProperty("nofollow")
    public void setNofollow(Boolean nofollow) {
        this.nofollow = nofollow;
    }

    @JsonProperty("count")
    public Integer getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(Integer count) {
        this.count = count;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

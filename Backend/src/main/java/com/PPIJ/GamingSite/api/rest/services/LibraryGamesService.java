package com.PPIJ.GamingSite.api.rest.services;

import com.PPIJ.GamingSite.api.dao.LibraryGamesRepository;
import com.PPIJ.GamingSite.api.model.LibraryGames;
import com.PPIJ.GamingSite.api.model.User;
import com.PPIJ.GamingSite.api.model.games.GamesByID;
import com.PPIJ.GamingSite.api.model.games.gameDetails.GameDetails;
import com.PPIJ.GamingSite.api.rest.client.GameDetailClient;
import com.PPIJ.GamingSite.api.rest.dtos.GamesDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class LibraryGamesService {
    @Autowired
    LibraryGamesRepository libraryGamesRepository;

    @Autowired
    GameDetailClient gameDetailClient;

    @Autowired
    GamesByIDService gamesByIDService;

    @Autowired
    WishlistGamesService wishlistGamesService;

    public List<GamesDTO> getLibraryGames(int libraryID){
        return libraryGamesRepository.findByLibraryID(libraryID)
                .stream()
                .map(t-> {
                    GamesDTO game = new GamesDTO();
                    game.setGameID(t.getGameID());
                    game.setGameTitle(t.getGameTitle());
                    return game;})
                .collect(Collectors.toList());
    }

    public String getGameFromLibraryByUser(User user){
        List<GamesDTO> libraryGames = getLibraryGames(user.getLibrary().getID());;
        if(libraryGames.size()!=0){
            Random rand = new Random();
            Integer gameID = libraryGames.get(rand.nextInt(libraryGames.size())).getGameID();
            GameDetails game = gameDetailClient.getGameDetails(gameID);
            return game.getSlug();
        }
        List<GamesDTO> wishlistGames = wishlistGamesService.getWishlistGames(user.getWishlist().getID());
        if(wishlistGames.size()!=0){
            Random rand = new Random();
            Integer gameID = wishlistGames.get(rand.nextInt(wishlistGames.size())).getGameID();
            GameDetails game = gameDetailClient.getGameDetails(gameID);
            return game.getSlug();
        }
        return "Prazna lista";
    }
}

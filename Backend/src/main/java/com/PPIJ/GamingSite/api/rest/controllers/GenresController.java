package com.PPIJ.GamingSite.api.rest.controllers;

import com.PPIJ.GamingSite.api.model.genres.Genre;
import com.PPIJ.GamingSite.api.model.genres.Result;
import com.PPIJ.GamingSite.api.model.genres.genreDetails.GenreDetails;
import com.PPIJ.GamingSite.api.model.platforms.Platforms;

import com.PPIJ.GamingSite.api.model.platforms.platformsDetails.PlatformDetails;
import com.PPIJ.GamingSite.api.rest.client.AllGenresClient;

import com.PPIJ.GamingSite.api.rest.client.GenreDetailClient;
import com.PPIJ.GamingSite.api.rest.client.PlatformDetailClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
@RequestMapping("/genres")
public class GenresController {

    @Autowired
    GenreDetailClient genreDetailClient;
    @Autowired
    AllGenresClient allGenresClient;

    @GetMapping("")
    public List<Result> getFirstFewGenres(){
        return allGenresClient.getFirstFewGenres();
    }

    @GetMapping("/{id}")
    public GenreDetails getGenreDetails(@PathVariable int id) {
        return genreDetailClient.getGenreDetails(id);
    }

    @GetMapping("/all")
    public Genre getAllGenres(){
        return allGenresClient.getAllGenres();
    }
}

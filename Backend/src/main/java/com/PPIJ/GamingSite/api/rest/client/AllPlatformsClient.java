package com.PPIJ.GamingSite.api.rest.client;

import com.PPIJ.GamingSite.api.model.games.ListOfGames;
import com.PPIJ.GamingSite.api.model.games.Platform;
import com.PPIJ.GamingSite.api.model.platforms.Platforms;
import com.PPIJ.GamingSite.api.model.platforms.Result;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class AllPlatformsClient {
    @Value("https://api.rawg.io/api/platforms?page=")
    String allPlatformsUrl;
    @Value("${rapidapi.key.name}")
    String apiKeyName;
    @Value("${rapidapi.key.value}")
    String apiKeyValue;
    @Value("${rapidapi.host.name}")
    String hostName;
    @Value("${rapidapi.host.games.value}")
    String hostValue;

    RestTemplate restTemplate;
    public AllPlatformsClient(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.build();
    }
    public Platforms getAllPlatforms(String page) {
        Platforms allPlatforms = null;
        try {
            URI uri;
            uri = new URI(allPlatformsUrl+page);
            HttpHeaders headers = new HttpHeaders();
            headers.set(apiKeyName, apiKeyValue);
            headers.set(hostName, hostValue);
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            HttpEntity<String> request = new HttpEntity<String>(headers);
            ResponseEntity<Platforms> totalEntity = restTemplate.exchange(uri, HttpMethod.GET, request,
                    Platforms.class);
            allPlatforms = totalEntity.getBody();

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return allPlatforms;
    }

    public List<Result> getFirstFewPlatforms(){
        Platforms allPlatforms = null;
        try {
            URI uri;
            uri = new URI(allPlatformsUrl+1);
            HttpHeaders headers = new HttpHeaders();
            headers.set(apiKeyName, apiKeyValue);
            headers.set(hostName, hostValue);
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            HttpEntity<String> request = new HttpEntity<String>(headers);
            ResponseEntity<Platforms> totalEntity = restTemplate.exchange(uri, HttpMethod.GET, request,
                    Platforms.class);
            allPlatforms = totalEntity.getBody();

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        List<Result> fewPlatforms = new ArrayList<>();
        fewPlatforms.add(allPlatforms.getResults().get(0));
        fewPlatforms.addAll(allPlatforms.getResults().subList(2,4));
        fewPlatforms.addAll(allPlatforms.getResults().subList(6,8));

        return fewPlatforms;
    }
}

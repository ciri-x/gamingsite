package com.PPIJ.GamingSite.api.dao;

import com.PPIJ.GamingSite.api.model.Library;
import com.PPIJ.GamingSite.api.model.Wishlist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LibraryRepository extends JpaRepository<Library, Integer> {
    Library findByID (int libraryId);
}

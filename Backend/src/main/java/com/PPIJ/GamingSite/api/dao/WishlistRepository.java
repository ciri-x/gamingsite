package com.PPIJ.GamingSite.api.dao;

import com.PPIJ.GamingSite.api.model.User;
import com.PPIJ.GamingSite.api.model.Wishlist;
import com.PPIJ.GamingSite.api.model.WishlistGames;
import com.PPIJ.GamingSite.api.model.games.GamesByID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface WishlistRepository extends JpaRepository<Wishlist, Integer> {
    Wishlist findByID(int wishlistID);
}
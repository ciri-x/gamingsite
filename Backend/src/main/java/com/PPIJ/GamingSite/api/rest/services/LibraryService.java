package com.PPIJ.GamingSite.api.rest.services;

import com.PPIJ.GamingSite.api.dao.LibraryGamesRepository;
import com.PPIJ.GamingSite.api.dao.LibraryRepository;
import com.PPIJ.GamingSite.api.model.*;
import com.PPIJ.GamingSite.exception.BadRequestException;
import com.PPIJ.GamingSite.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LibraryService {
    @Autowired
    LibraryRepository libraryRepository;

    @Autowired
    LibraryGamesRepository libraryGamesRepository;

    public Library getLibrary(int libraryID) { return libraryRepository.findByID(libraryID); }



    public void saveGame(int gameID,String gameTitle, User user) {

        Library library = getLibrary(user.getLibrary().getID());

        if (libraryGamesRepository.findByGameIDAndLibraryID(gameID, library.getID()).isPresent()) {
            throw new BadRequestException("Game already in wishlist.");
        }


        LibraryGames game = new LibraryGames(gameID, gameTitle);

        game.setLibrary(libraryRepository.findByID(library.getID()));

        libraryGamesRepository.save(game);
    }
    public void removeGame(int gameID, User user) {

        Library library = getLibrary(user.getLibrary().getID());

        LibraryGames games = libraryGamesRepository.findByGameIDAndLibraryID(gameID, library.getID()).orElseThrow(() -> new NotFoundException("Game not found"));

        libraryGamesRepository.deleteById(games.getID());
    }
}

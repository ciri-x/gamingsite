package com.PPIJ.GamingSite.api.rest.client;

import com.PPIJ.GamingSite.api.model.genres.Genre;
import com.PPIJ.GamingSite.api.model.genres.Result;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;
@Component
public class AllGenresClient {
    @Value("https://rawg-video-games-database.p.rapidapi.com/genres")
    String allPlatformsUrl;
    @Value("${rapidapi.key.name}")
    String apiKeyName;
    @Value("${rapidapi.key.value}")
    String apiKeyValue;
    @Value("${rapidapi.host.name}")
    String hostName;
    @Value("${rapidapi.host.games.value}")
    String hostValue;

    RestTemplate restTemplate;
    public AllGenresClient(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.build();
    }
    public Genre getAllGenres() {
        Genre allGenres = null;
        try {
            URI uri;
            uri = new URI(allPlatformsUrl);
            HttpHeaders headers = new HttpHeaders();
            headers.set(apiKeyName, apiKeyValue);
            headers.set(hostName, hostValue);
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            HttpEntity<String> request = new HttpEntity<String>(headers);
            ResponseEntity<Genre> totalEntity = restTemplate.exchange(uri, HttpMethod.GET, request,
                    Genre.class);
            allGenres = totalEntity.getBody();

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return allGenres;
    }

    public List<Result> getFirstFewGenres(){
        Genre firstFewGenres = null;
        try {
            URI uri;
            uri = new URI(allPlatformsUrl);
            HttpHeaders headers = new HttpHeaders();
            headers.set(apiKeyName, apiKeyValue);
            headers.set(hostName, hostValue);
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            HttpEntity<String> request = new HttpEntity<String>(headers);
            ResponseEntity<Genre> totalEntity = restTemplate.exchange(uri, HttpMethod.GET, request,
                    Genre.class);
            firstFewGenres = totalEntity.getBody();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return firstFewGenres.getResults().subList(0,5);
    }
}

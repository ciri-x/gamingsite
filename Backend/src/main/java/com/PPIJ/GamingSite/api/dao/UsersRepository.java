package com.PPIJ.GamingSite.api.dao;

import com.PPIJ.GamingSite.api.model.User;
import com.PPIJ.GamingSite.api.model.Wishlist;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Integer> {
    Optional<User> findByUsername(String username);
    Optional<User> findByEmail(String email);
    User findByConfirmationToken(String confirmationToken);
    Wishlist findByWishlist(Wishlist wishlist);
}

package com.PPIJ.GamingSite.api.rest.controllers;

import com.PPIJ.GamingSite.api.model.User;
import com.PPIJ.GamingSite.api.rest.dtos.CreateUserDTO;
import com.PPIJ.GamingSite.api.rest.services.EmailSenderService;
import com.PPIJ.GamingSite.api.rest.services.UsersService;
import com.PPIJ.GamingSite.exception.ForbiddenException;
import com.PPIJ.GamingSite.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.*;
import java.util.UUID;

@RestController
@RequestMapping("/registration")
public class RegistrationController {

    @Autowired
    private UsersService usersService;

    @Autowired
    private EmailSenderService emailSenderService;

    @PostMapping("")
    public User register(@RequestBody CreateUserDTO user) {

        User newUser = usersService.saveUser(user.getUsername(), user.getPassword(), user.getFirstName(), user.getLastName(),
                user.getEmail(), user.getRole(), false, UUID.randomUUID().toString());

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(newUser.getEmail());
        mailMessage.setSubject("Complete Registration!");
        mailMessage.setFrom("lozinka.zavrsni@gmail.com");
        mailMessage.setText("Za potvrdu računa otvorite link i upišite aktivacijski kod : "
                +"https://lozinka.herokuapp.com/potvrdaRegistracije "
                +"Aktivacijski kod: " + newUser.getConfirmationToken());

        emailSenderService.sendEmail(mailMessage);
        return newUser;
    }

    @PutMapping("/confirm")
    public void confirmRegistration(@RequestParam String token){
        User confirmedUser = usersService.findByConfirmationToken(token);

        if(confirmedUser == null){
            throw new ForbiddenException("No user found");
        }
        else{
            usersService.updateUser(confirmedUser);
        }
    }

    @PostMapping("/changePassword")
    public User changePasswordRequest(@RequestParam String email) {
        User confirmedUser = usersService.getUserByEmail(email).orElseThrow(() -> new NotFoundException("User not found"));

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(email);
        mailMessage.setSubject("Promjena lozinke!");
        mailMessage.setFrom("lozinka.zavrsni@gmail.com");
        mailMessage.setText("Za promjenu lozinke otvorite navedeni link : "
                //+"http://localhost:3000/promjenaLozinke"
                +"https://lozinka.herokuapp.com/promjenaLozinke"
                +" Kod za promjenu lozinke: " + confirmedUser.getConfirmationToken());

        emailSenderService.sendEmail(mailMessage);
        return confirmedUser;
    }

    @PutMapping("/confirmChange")
    public void confirmChange(@RequestParam String newPassword,@RequestParam String token){
        User confirmedUser = usersService.findByConfirmationToken(token);

        if(confirmedUser == null){
            throw new ForbiddenException("No user found");
        }
        else{
            usersService.updateUserPassword(confirmedUser, newPassword);
        }
    }

    @PutMapping("/changeUsername")
    public void changeUserName(@RequestParam String newUsername, @RequestParam String email){
        User confirmedUser = usersService.getUserByEmail(email).orElseThrow(() -> new NotFoundException("User not found"));

        if(confirmedUser == null){
            throw new ForbiddenException("No user found");
        }
        else{
            usersService.updateUsername(confirmedUser, newUsername);
        }
    }

    @PutMapping("/changePasswordProfile")
    public void changePassword(@RequestParam String newPassword, @RequestParam String email){
        User confirmedUser = usersService.getUserByEmail(email).orElseThrow(() -> new NotFoundException("User not found"));

        if(confirmedUser == null){
            throw new ForbiddenException("No user found");
        }
        else{
            usersService.updateUserPassword(confirmedUser, newPassword);
        }
    }


}
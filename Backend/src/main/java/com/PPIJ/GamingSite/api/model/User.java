package com.PPIJ.GamingSite.api.model;

import com.PPIJ.GamingSite.api.model.enums.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.StringUtils;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "person")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int ID;

    @Column(unique = true)
    private String username;

    private String password;

    private String firstName;

    private String lastName;

    @Column(unique = true)
    private String email;

    @Enumerated(EnumType.STRING)
    private Role role;

    private boolean isConfirmed;

    private String confirmationToken;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn
    private Wishlist wishlist;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn
    private Library library;


    public User(String username, String password, String firstName, String lastName,
                String email, Role role, boolean isConfirmed, String confirmationToken, Wishlist wishlist, Library library) {
        this.username = username;
        setPassword(password);
        this.firstName = StringUtils.capitalize(firstName);
        this.lastName = StringUtils.capitalize(lastName);
        this.email = email;
        this.role = role;
        this.isConfirmed = isConfirmed;
        this.confirmationToken = confirmationToken;
        this.wishlist = wishlist;
        this.library=library;
    }

    public void setPassword(String password) {
        this.password = new BCryptPasswordEncoder().encode(password);
    }
}


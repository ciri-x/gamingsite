
package com.PPIJ.GamingSite.api.model.games.gameDetails;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "owned",
    "beaten",
    "toplay"
})
public class AddedByStatus {

    @JsonProperty("owned")
    private Integer owned;
    @JsonProperty("beaten")
    private Integer beaten;
    @JsonProperty("toplay")
    private Integer toplay;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("owned")
    public Integer getOwned() {
        return owned;
    }

    @JsonProperty("owned")
    public void setOwned(Integer owned) {
        this.owned = owned;
    }

    @JsonProperty("beaten")
    public Integer getBeaten() {
        return beaten;
    }

    @JsonProperty("beaten")
    public void setBeaten(Integer beaten) {
        this.beaten = beaten;
    }

    @JsonProperty("toplay")
    public Integer getToplay() {
        return toplay;
    }

    @JsonProperty("toplay")
    public void setToplay(Integer toplay) {
        this.toplay = toplay;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

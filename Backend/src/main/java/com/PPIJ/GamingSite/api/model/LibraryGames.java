package com.PPIJ.GamingSite.api.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "library_games")
public class LibraryGames {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int ID;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "library_id")
    private Library library;

    private Integer gameID;

    private String gameTitle;

    public LibraryGames(Integer gameID, String gameTitle) {
        this.gameID= gameID;
        this.gameTitle = gameTitle;
    }
}

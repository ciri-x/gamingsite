package com.PPIJ.GamingSite.api.rest.controllers;


import com.PPIJ.GamingSite.api.model.Wishlist;
import com.PPIJ.GamingSite.api.model.WishlistGames;
import com.PPIJ.GamingSite.api.model.recommended.Result;
import com.PPIJ.GamingSite.api.rest.client.AllRecommendedClient;
import com.PPIJ.GamingSite.api.rest.dtos.GamesDTO;
import com.PPIJ.GamingSite.api.rest.services.*;
import com.PPIJ.GamingSite.exception.ForbiddenException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.PPIJ.GamingSite.exception.NotFoundException;
import com.PPIJ.GamingSite.api.model.User;
import com.PPIJ.GamingSite.util.AuthorizationUtil;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UsersService usersService;

    @Autowired
    private WishlistService wishlistService;

    @Autowired
    private WishlistGamesService wishlistGamesService;

    @Autowired
    private LibraryGamesService libraryGamesService;

    @Autowired
    private LibraryService libraryService;

    @Autowired
    private AllRecommendedClient allRecommendedClient;

    @Autowired
    private AuthorizationUtil authorizationUtil;

    @GetMapping("")
    //@Secured("ROLE_ADMIN")
    public List<User> getAllUsers() {
        return usersService.getUsers();
    }

    @GetMapping("/{id}")
    public User getUser(@PathVariable int id) {
        return usersService.getUser(id).orElseThrow(() -> new NotFoundException("User not found"));
    }

    @DeleteMapping("/{id}")
    //@Secured("ROLE_ADMIN")
    public void deleteUser(@PathVariable int id) {
        usersService.deleteUser(id);
    }

    @PutMapping("/addToWishlist")
    public void addToWishlist(int gameID, String gameTitle, int userID){
        User confirmedUser = usersService.getUser(userID).orElseThrow(() -> new NotFoundException("User not found"));

        if(confirmedUser == null){
            throw new ForbiddenException("No user found");
        }
        else{
            wishlistService.saveGame(gameID,gameTitle, confirmedUser);
        }
    }

    @DeleteMapping("/removeFromWishlist")
    //@Secured("ROLE_ADMIN")
    public void removeFromWishlist(int gameID, int userID) {
        User confirmedUser = usersService.getUser(userID).orElseThrow(() -> new NotFoundException("User not found"));
        if(confirmedUser == null){
            throw new ForbiddenException("No user found");
        }
        else{
            wishlistService.removeGame(gameID, confirmedUser);
        }
    }

    @GetMapping("/getWishlist")
    public List<GamesDTO> getWishlist(int userID) {
        User confirmedUser = usersService.getUser(userID).orElseThrow(() -> new NotFoundException("User not found"));
        return wishlistGamesService.getWishlistGames(confirmedUser.getWishlist().getID());
    }
    @PutMapping("/addToLibrary")
    public void addToLibrary(int gameID, String gameTitle, int userID){
        User confirmedUser = usersService.getUser(userID).orElseThrow(() -> new NotFoundException("User not found"));

        if(confirmedUser == null){
            throw new ForbiddenException("No user found");
        }
        else{
            libraryService.saveGame(gameID,gameTitle , confirmedUser);
        }
    }

    @DeleteMapping("/removeFromLibrary")
    //@Secured("ROLE_ADMIN")
    public void removeFromLibrary(int gameID, int userID) {
        User confirmedUser = usersService.getUser(userID).orElseThrow(() -> new NotFoundException("User not found"));
        if(confirmedUser == null){
            throw new ForbiddenException("No user found");
        }
        else{
            libraryService.removeGame(gameID, confirmedUser);
        }
    }

    @GetMapping("/getLibrary")
    public List<GamesDTO> getLibrary(int userID) {
        User confirmedUser = usersService.getUser(userID).orElseThrow(() -> new NotFoundException("User not found"));
        return libraryGamesService.getLibraryGames(confirmedUser.getLibrary().getID());
    }

    @GetMapping("/getRecommendedByLibrary")
    public List<GamesDTO> getRecommendedByLibrary(int userID) {
        User confirmedUser = usersService.getUser(userID).orElseThrow(() -> new NotFoundException("User not found"));

        return allRecommendedClient.getAllRecommendedGames(libraryGamesService.getGameFromLibraryByUser(confirmedUser));
    }


}

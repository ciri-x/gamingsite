package com.PPIJ.GamingSite.api.rest.client;

import com.PPIJ.GamingSite.api.model.Library;
import com.PPIJ.GamingSite.api.model.User;
import com.PPIJ.GamingSite.api.model.platforms.Platforms;
import com.PPIJ.GamingSite.api.model.recommended.Recommended;
import com.PPIJ.GamingSite.api.model.recommended.Result;
import com.PPIJ.GamingSite.api.rest.dtos.GamesDTO;
import com.PPIJ.GamingSite.api.rest.services.GamesByIDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class AllRecommendedClient {


    @Value("${rapidapi.key.name}")
    String apiKeyName;
    @Value("${rapidapi.key.value}")
    String apiKeyValue;
    @Value("${rapidapi.host.name}")
    String hostName;
    @Value("${rapidapi.host.games.value}")
    String hostValue;

    @Autowired
    GamesByIDService gamesByIDService;

    RestTemplate restTemplate;
    public AllRecommendedClient(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.build();
    }
    public List<GamesDTO> getAllRecommendedGames(String slug) {
        if(slug.equals("Prazna lista")){
            return gamesByIDService.getGamesSortedByRating()
                    .subList(0,5)
                    .stream()
                    .map(t-> {
                        GamesDTO game = new GamesDTO();
                        game.setGameID(t.getGameID());
                        game.setGameTitle(t.getGameTitle());
                        return game;})
                            .collect(Collectors.toList());
        }
        Recommended allRecommended = null;
        try {
            URI uri;
            uri = new URI("https://rawg.io/api/games/"+slug+"/suggested?page=1");
            HttpHeaders headers = new HttpHeaders();
            headers.set(apiKeyName, apiKeyValue);
            headers.set(hostName, hostValue);
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            HttpEntity<String> request = new HttpEntity<String>(headers);
            ResponseEntity<Recommended> totalEntity = restTemplate.exchange(uri, HttpMethod.GET, request,
                    Recommended.class);
            allRecommended = totalEntity.getBody();

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return allRecommended.getResults().stream()
                .map(t-> {
                        GamesDTO game = new GamesDTO();
                        game.setGameID(t.getId());
                        game.setGameTitle(t.getName());
                        return game;})
                .collect(Collectors.toList());
    }

}

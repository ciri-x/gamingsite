package com.PPIJ.GamingSite.api.rest.services;

import com.PPIJ.GamingSite.api.dao.WishlistGamesRepository;
import com.PPIJ.GamingSite.api.model.WishlistGames;
import com.PPIJ.GamingSite.api.rest.dtos.GamesDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class WishlistGamesService {
    @Autowired
    WishlistGamesRepository wishlistGamesRepository;

    public List<GamesDTO> getWishlistGames(int wishlistID){
        return wishlistGamesRepository.findByWishlistID(wishlistID)
                .stream()
                .map(t-> {
                    GamesDTO game = new GamesDTO();
                    game.setGameID(t.getGameID());
                    game.setGameTitle(t.getGameTitle());
                    return game;})
                .collect(Collectors.toList());
    }
}

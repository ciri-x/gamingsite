
package com.PPIJ.GamingSite.api.model.recommended;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "minimum",
    "recommended"
})
public class RequirementsEn {

    @JsonProperty("minimum")
    private String minimum;
    @JsonProperty("recommended")
    private String recommended;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("minimum")
    public String getMinimum() {
        return minimum;
    }

    @JsonProperty("minimum")
    public void setMinimum(String minimum) {
        this.minimum = minimum;
    }

    @JsonProperty("recommended")
    public String getRecommended() {
        return recommended;
    }

    @JsonProperty("recommended")
    public void setRecommended(String recommended) {
        this.recommended = recommended;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

package com.PPIJ.GamingSite.api.rest.services;


import com.PPIJ.GamingSite.api.dao.UsersRepository;

import com.PPIJ.GamingSite.api.dao.WishlistGamesRepository;
import com.PPIJ.GamingSite.api.dao.WishlistRepository;
import com.PPIJ.GamingSite.api.model.User;
import com.PPIJ.GamingSite.api.model.Wishlist;
import com.PPIJ.GamingSite.api.model.WishlistGames;
import com.PPIJ.GamingSite.api.model.games.GamesByID;
import com.PPIJ.GamingSite.exception.BadRequestException;
import com.PPIJ.GamingSite.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WishlistService {
    @Autowired
    private WishlistRepository wishlistRepository;

    @Autowired
    private WishlistGamesRepository wishlistGamesRepository;

    @Autowired
    private UsersRepository usersRepository;

    public Wishlist getWishlist(int wishlistID) { return wishlistRepository.findByID(wishlistID); }

    public void saveGame(int gameID,String gameTitle, User user) {

        Wishlist wishlist = getWishlist(user.getWishlist().getID());

        if (wishlistGamesRepository.findByGameIDAndWishlistID(gameID, wishlist.getID()).isPresent()) {
            throw new BadRequestException("Game already in wishlist.");
        }


        WishlistGames game = new WishlistGames(gameID, gameTitle);

        game.setWishlist(wishlistRepository.findByID(wishlist.getID()));

        wishlistGamesRepository.save(game);

    }

    public void removeGame(int gameID, User user) {

        Wishlist wishlist = getWishlist(user.getWishlist().getID());

       WishlistGames games = wishlistGamesRepository.findByGameIDAndWishlistID(gameID, wishlist.getID()).orElseThrow(() -> new NotFoundException("Game not found"));

        wishlistGamesRepository.deleteById(games.getID());

    }
}

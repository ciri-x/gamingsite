package com.PPIJ.GamingSite.api.rest.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class GamesDTO {
    private Integer gameID;
    private String gameTitle;
}

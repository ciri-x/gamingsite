
package com.PPIJ.GamingSite.api.model.recommended;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "store",
    "url_en",
    "url_ru"
})
public class Store {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("store")
    private Store_ store;
    @JsonProperty("url_en")
    private String urlEn;
    @JsonProperty("url_ru")
    private Object urlRu;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("store")
    public Store_ getStore() {
        return store;
    }

    @JsonProperty("store")
    public void setStore(Store_ store) {
        this.store = store;
    }

    @JsonProperty("url_en")
    public String getUrlEn() {
        return urlEn;
    }

    @JsonProperty("url_en")
    public void setUrlEn(String urlEn) {
        this.urlEn = urlEn;
    }

    @JsonProperty("url_ru")
    public Object getUrlRu() {
        return urlRu;
    }

    @JsonProperty("url_ru")
    public void setUrlRu(Object urlRu) {
        this.urlRu = urlRu;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

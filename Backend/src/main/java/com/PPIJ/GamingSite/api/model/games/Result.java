
package com.PPIJ.GamingSite.api.model.games;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "slug",
    "name",
    "released",
    "tba",
    "background_image",
    "rating",
    "rating_top",
    "ratings",
    "ratings_count",
    "reviews_text_count",
    "added",
    "added_by_status",
    "metacritic",
    "playtime",
    "suggestions_count",
    "user_game",
    "reviews_count",
    "saturated_color",
    "dominant_color",
    "platforms",
    "parent_platforms",
    "genres",
    "stores",
    "clip",
    "tags",
    "short_screenshots"
})
public class Result {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("slug")
    private String slug;
    @JsonProperty("name")
    private String name;
    @JsonProperty("released")
    private String released;
    @JsonProperty("tba")
    private Boolean tba;
    @JsonProperty("background_image")
    private String backgroundImage;
    @JsonProperty("rating")
    private Double rating;
    @JsonProperty("rating_top")
    private Integer ratingTop;
    @JsonProperty("ratings")
    private List<Rating> ratings = null;
    @JsonProperty("ratings_count")
    private Integer ratingsCount;
    @JsonProperty("reviews_text_count")
    private Integer reviewsTextCount;
    @JsonProperty("added")
    private Integer added;
    @JsonProperty("added_by_status")
    private AddedByStatus addedByStatus;
    @JsonProperty("metacritic")
    private Integer metacritic;
    @JsonProperty("playtime")
    private Integer playtime;
    @JsonProperty("suggestions_count")
    private Integer suggestionsCount;
    @JsonProperty("user_game")
    private Object userGame;
    @JsonProperty("reviews_count")
    private Integer reviewsCount;
    @JsonProperty("saturated_color")
    private String saturatedColor;
    @JsonProperty("dominant_color")
    private String dominantColor;
    @JsonProperty("platforms")
    private List<Platform> platforms = null;
    @JsonProperty("parent_platforms")
    private List<ParentPlatform> parentPlatforms = null;
    @JsonProperty("genres")
    private List<Genre> genres = null;
    @JsonProperty("stores")
    private List<Store> stores = null;
    @JsonProperty("clip")
    private Clip clip;
    @JsonProperty("tags")
    private List<Tag> tags = null;
    @JsonProperty("short_screenshots")
    private List<ShortScreenshot> shortScreenshots = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("slug")
    public String getSlug() {
        return slug;
    }

    @JsonProperty("slug")
    public void setSlug(String slug) {
        this.slug = slug;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("released")
    public String getReleased() {
        return released;
    }

    @JsonProperty("released")
    public void setReleased(String released) {
        this.released = released;
    }

    @JsonProperty("tba")
    public Boolean getTba() {
        return tba;
    }

    @JsonProperty("tba")
    public void setTba(Boolean tba) {
        this.tba = tba;
    }

    @JsonProperty("background_image")
    public String getBackgroundImage() {
        return backgroundImage;
    }

    @JsonProperty("background_image")
    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    @JsonProperty("rating")
    public Double getRating() {
        return rating;
    }

    @JsonProperty("rating")
    public void setRating(Double rating) {
        this.rating = rating;
    }

    @JsonProperty("rating_top")
    public Integer getRatingTop() {
        return ratingTop;
    }

    @JsonProperty("rating_top")
    public void setRatingTop(Integer ratingTop) {
        this.ratingTop = ratingTop;
    }

    @JsonProperty("ratings")
    public List<Rating> getRatings() {
        return ratings;
    }

    @JsonProperty("ratings")
    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

    @JsonProperty("ratings_count")
    public Integer getRatingsCount() {
        return ratingsCount;
    }

    @JsonProperty("ratings_count")
    public void setRatingsCount(Integer ratingsCount) {
        this.ratingsCount = ratingsCount;
    }

    @JsonProperty("reviews_text_count")
    public Integer getReviewsTextCount() {
        return reviewsTextCount;
    }

    @JsonProperty("reviews_text_count")
    public void setReviewsTextCount(Integer reviewsTextCount) {
        this.reviewsTextCount = reviewsTextCount;
    }

    @JsonProperty("added")
    public Integer getAdded() {
        return added;
    }

    @JsonProperty("added")
    public void setAdded(Integer added) {
        this.added = added;
    }

    @JsonProperty("added_by_status")
    public AddedByStatus getAddedByStatus() {
        return addedByStatus;
    }

    @JsonProperty("added_by_status")
    public void setAddedByStatus(AddedByStatus addedByStatus) {
        this.addedByStatus = addedByStatus;
    }

    @JsonProperty("metacritic")
    public Integer getMetacritic() {
        return metacritic;
    }

    @JsonProperty("metacritic")
    public void setMetacritic(Integer metacritic) {
        this.metacritic = metacritic;
    }

    @JsonProperty("playtime")
    public Integer getPlaytime() {
        return playtime;
    }

    @JsonProperty("playtime")
    public void setPlaytime(Integer playtime) {
        this.playtime = playtime;
    }

    @JsonProperty("suggestions_count")
    public Integer getSuggestionsCount() {
        return suggestionsCount;
    }

    @JsonProperty("suggestions_count")
    public void setSuggestionsCount(Integer suggestionsCount) {
        this.suggestionsCount = suggestionsCount;
    }

    @JsonProperty("user_game")
    public Object getUserGame() {
        return userGame;
    }

    @JsonProperty("user_game")
    public void setUserGame(Object userGame) {
        this.userGame = userGame;
    }

    @JsonProperty("reviews_count")
    public Integer getReviewsCount() {
        return reviewsCount;
    }

    @JsonProperty("reviews_count")
    public void setReviewsCount(Integer reviewsCount) {
        this.reviewsCount = reviewsCount;
    }

    @JsonProperty("saturated_color")
    public String getSaturatedColor() {
        return saturatedColor;
    }

    @JsonProperty("saturated_color")
    public void setSaturatedColor(String saturatedColor) {
        this.saturatedColor = saturatedColor;
    }

    @JsonProperty("dominant_color")
    public String getDominantColor() {
        return dominantColor;
    }

    @JsonProperty("dominant_color")
    public void setDominantColor(String dominantColor) {
        this.dominantColor = dominantColor;
    }

    @JsonProperty("platforms")
    public List<Platform> getPlatforms() {
        return platforms;
    }

    @JsonProperty("platforms")
    public void setPlatforms(List<Platform> platforms) {
        this.platforms = platforms;
    }

    @JsonProperty("parent_platforms")
    public List<ParentPlatform> getParentPlatforms() {
        return parentPlatforms;
    }

    @JsonProperty("parent_platforms")
    public void setParentPlatforms(List<ParentPlatform> parentPlatforms) {
        this.parentPlatforms = parentPlatforms;
    }

    @JsonProperty("genres")
    public List<Genre> getGenres() {
        return genres;
    }

    @JsonProperty("genres")
    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    @JsonProperty("stores")
    public List<Store> getStores() {
        return stores;
    }

    @JsonProperty("stores")
    public void setStores(List<Store> stores) {
        this.stores = stores;
    }

    @JsonProperty("clip")
    public Clip getClip() {
        return clip;
    }

    @JsonProperty("clip")
    public void setClip(Clip clip) {
        this.clip = clip;
    }

    @JsonProperty("tags")
    public List<Tag> getTags() {
        return tags;
    }

    @JsonProperty("tags")
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    @JsonProperty("short_screenshots")
    public List<ShortScreenshot> getShortScreenshots() {
        return shortScreenshots;
    }

    @JsonProperty("short_screenshots")
    public void setShortScreenshots(List<ShortScreenshot> shortScreenshots) {
        this.shortScreenshots = shortScreenshots;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

package com.PPIJ.GamingSite.api.rest.client;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.PPIJ.GamingSite.api.model.games.GamesByID;
import com.PPIJ.GamingSite.api.model.games.Result;
import com.PPIJ.GamingSite.api.model.genres.Game;

import com.PPIJ.GamingSite.api.rest.services.UsersService;
import com.PPIJ.GamingSite.util.AuthorizationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import com.PPIJ.GamingSite.api.model.games.ListOfGames;

@Component
public class AllGamesRestClient {

    @Value("https://api.rawg.io/api/games?page=")
    String allGamesUrl;
    @Value("${rapidapi.key.name}")
    String apiKeyName;
    @Value("${rapidapi.key.value}")
    String apiKeyValue;
    @Value("${rapidapi.host.name}")
    String hostName;
    @Value("${rapidapi.host.games.value}")
    String hostValue;


    RestTemplate restTemplate;
    public AllGamesRestClient(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.build();
    }
    public ListOfGames getAllGames(String page) {
        ListOfGames allGames = null;
        try {
            URI uri;
            uri = new URI(allGamesUrl+page);
            HttpHeaders headers = new HttpHeaders();
            headers.set(apiKeyName, apiKeyValue);
            headers.set(hostName, hostValue);
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            HttpEntity<String> request = new HttpEntity<String>(headers);
            ResponseEntity<ListOfGames> totalEntity = restTemplate.exchange(uri, HttpMethod.GET, request,
                    ListOfGames.class);
            allGames = totalEntity.getBody();

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return allGames;
    }

    public List<GamesByID> getAllGamesByID(Integer firstPage, Integer lastPage) {
        ListOfGames allGames = null;
        List<GamesByID> list=new ArrayList<>();
        int i = firstPage;
        do{
            try {
                URI uri;
                uri = new URI(allGamesUrl+i);
                HttpHeaders headers = new HttpHeaders();
                headers.set(apiKeyName, apiKeyValue);
                headers.set(hostName, hostValue);
                headers.setContentType(MediaType.APPLICATION_JSON);
                headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
                HttpEntity<String> request = new HttpEntity<String>(headers);
                ResponseEntity<ListOfGames> totalEntity = restTemplate.exchange(uri, HttpMethod.GET, request,
                        ListOfGames.class);
                allGames = totalEntity.getBody();

                for(Result result : allGames.getResults()){
                    GamesByID game = new GamesByID(result.getId(),result.getName(), result.getRating());
                    list.add(game);
                }
                i++;
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }while(firstPage<i && i<=lastPage);

        return list;
    }

}

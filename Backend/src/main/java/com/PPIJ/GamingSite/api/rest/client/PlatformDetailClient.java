package com.PPIJ.GamingSite.api.rest.client;

import com.PPIJ.GamingSite.api.model.platforms.platformsDetails.PlatformDetails;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
@Component
public class PlatformDetailClient {

    @Value("https://rawg-video-games-database.p.rapidapi.com/platforms/")
    String allGamesUrl;
    @Value("${rapidapi.key.name}")
    String apiKeyName;
    @Value("${rapidapi.key.value}")
    String apiKeyValue;
    @Value("${rapidapi.host.name}")
    String hostName;
    @Value("${rapidapi.host.games.value}")
    String hostValue;

    RestTemplate restTemplate;
    public PlatformDetailClient(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.build();
    }
    public PlatformDetails getPlatformDetails(int id) {
        PlatformDetails details = null;
        try {
            URI uri;
            uri = new URI(allGamesUrl+id);
            HttpHeaders headers = new HttpHeaders();
            headers.set(apiKeyName, apiKeyValue);
            headers.set(hostName, hostValue);
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            HttpEntity<String> request = new HttpEntity<String>(headers);
            ResponseEntity<PlatformDetails> totalEntity = restTemplate.exchange(uri, HttpMethod.GET, request,
                    PlatformDetails.class);
            details = totalEntity.getBody();

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return details;
    }

}

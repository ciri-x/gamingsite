package com.PPIJ.GamingSite.api.dao;

import com.PPIJ.GamingSite.api.model.WishlistGames;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface WishlistGamesRepository extends JpaRepository<WishlistGames, Integer> {
    List<WishlistGames> findByWishlistID(int wishlistID);
    Optional<WishlistGames> findByGameIDAndWishlistID(int gameID, int wishlistID);
}

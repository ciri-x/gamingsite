import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup } from '@angular/forms';
import { REGISTER, CONFIRM, LOGIN } from '../service/service';
import { LocalStorage } from '../service/localstorage';

@Injectable({
    providedIn: 'root'
})
export class UserService{

    constructor(private http: HttpClient,private user:LocalStorage){
    }

    registerUser(registerForm: FormGroup){
        return this.http.post(REGISTER,{
            username: registerForm.get('username').value,
            password: registerForm.get('password').value,
            firstName: registerForm.get('firstName').value,
            lastName: registerForm.get('lastName').value,
            email: registerForm.get('email').value,
            role: "CITIZEN"

        });

    }

    confirmToken(confirmForm: FormGroup){
        const asd = CONFIRM + confirmForm.get('token').value;
        return this.http.put(asd,{
            token: confirmForm.get('token').value
        })
    }

    loginUser(loginForm: FormGroup){
        return this.http.post(LOGIN,{
            username: loginForm.get('username').value,
            password: loginForm.get('password').value
        });
    }

    logOut(){
        this.user.clearUser();
}
}
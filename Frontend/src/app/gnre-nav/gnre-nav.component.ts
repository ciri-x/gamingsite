import { Component, OnInit, Input } from '@angular/core';
import { Genre } from '../models/Genre';
import { GenreService } from '../service/genreService';
import { Router } from '@angular/router';
import { LocalStorage } from '../service/localstorage';
import { GenreDetails } from '../models/GenreDetails';

@Component({
  selector: 'app-gnre-nav',
  templateUrl: './gnre-nav.component.html',
  styleUrls: ['./gnre-nav.component.css']
})
export class GnreNavComponent implements OnInit {

  @Input() genre: Genre;

  constructor(private service:GenreService,private router:Router,private localstorage:LocalStorage) { }

  ngOnInit(): void {
  }

  getGenreDetails(id:number){
    this.service.getGenreDetails(id).subscribe((response:GenreDetails)=>{
      this.localstorage.setGenreDetails(response);
      this.router.navigate(['/genreDetails']).then(()=>{
        window.location.reload();
      });
    })

}
}

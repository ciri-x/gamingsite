import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GnreNavComponent } from './gnre-nav.component';

describe('GnreNavComponent', () => {
  let component: GnreNavComponent;
  let fixture: ComponentFixture<GnreNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GnreNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GnreNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

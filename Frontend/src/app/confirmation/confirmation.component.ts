import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UserService } from '../user-service/user-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.css']
})
export class ConfirmationComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) { }

  confirmForm = new FormGroup({
    token: new FormControl('')
  })


  
  ngOnInit() {
    }
  confirmToken(confirmForm: FormGroup){
    this.userService.confirmToken(confirmForm).subscribe((response) =>{
    console.log(response);
    this.router.navigate(['/login']);
  })
  }
}

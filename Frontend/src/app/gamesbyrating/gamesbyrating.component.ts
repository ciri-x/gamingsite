import { Component, OnInit, PipeTransform, ViewChildren, QueryList } from '@angular/core';
import { GameService } from '../service/gameService';
import { GameRaitingDetails } from '../models/GameRaitingDetails';
import { SortColumn, SortDirection, NgbdSortableHeader, SortEvent } from '../service/sortable.directive';
import { BehaviorSubject, Subject, Observable, of } from 'rxjs';
import { GamebyID } from '../models/Game-byID';
import { Router } from '@angular/router';
import { DecimalPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { LocalStorage } from '../service/localstorage';
import { tap, debounceTime, switchMap, delay } from 'rxjs/operators';
interface SearchResult {
  games: GameRaitingDetails[];
  total: number;
}
interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: SortColumn;
  sortDirection: SortDirection;
}
const compare = (v1: string, v2: string) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
function sort(games: GameRaitingDetails[], column: SortColumn, direction: string): GameRaitingDetails[] {
  if (direction === '' || column === '') {
    return games;
  } else {
    return [...games].sort((a, b) => {
      const res = compare(`${a[column]}`, `${b[column]}`);
      return direction === 'asc' ? res : -res;
    });
  }
}
function matches(game: GameRaitingDetails, term: string, pipe: PipeTransform) {
  return game.gameTitle.toLowerCase().includes(term.toLowerCase());
}

@Component({
  selector: 'app-gamesbyrating',
  templateUrl: './gamesbyrating.component.html',
  styleUrls: ['./gamesbyrating.component.css']
})
export class GamesbyratingComponent {
  private _state: State = {
    page: 1,
    pageSize: 30,
    searchTerm: '',
    sortColumn: '',
    sortDirection: ''
  };
  private _loading$ = new BehaviorSubject<boolean>(true);
  private _search$ = new Subject<void>();
  private _games$ = new BehaviorSubject<GameRaitingDetails[]>([]);
  private _total$ = new BehaviorSubject<number>(0);

  allGames : GameRaitingDetails[];
  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;
  constructor(private gameService:GameService, private router: Router ,private pipe: DecimalPipe, private htttp:HttpClient,private localstorage:LocalStorage) {
    this.allGames=localstorage.getGamesRating();
    console.log(this.allGames);
    this._search$.pipe(
      tap(() => this._loading$.next(true)),
      debounceTime(200),
      switchMap(() => this._search()),
      delay(200),
      tap(() => this._loading$.next(false))
    ).subscribe(result => {
      this._games$.next(result.games);
      this._total$.next(result.total);
    });
    this._search$.next();
  }
  get games$() { return this._games$.asObservable(); }
  get total$() { return this._total$.asObservable(); }
  get loading$() { return this._loading$.asObservable(); }
  get page() { return this._state.page; }
  get pageSize() { return this._state.pageSize; }
  get searchTerm() { return this._state.searchTerm; }

  set page(page: number) { this._set({page}); }
  set pageSize(pageSize: number) { this._set({pageSize}); }
  set searchTerm(searchTerm: string) { this._set({searchTerm}); }
  set sortColumn(sortColumn: SortColumn) { this._set({sortColumn}); }
  set sortDirection(sortDirection: SortDirection) { this._set({sortDirection}); }
  private _set(patch: Partial<State>) {
    Object.assign(this._state, patch);
    this._search$.next();
  }
  private _search(): Observable<SearchResult> {
    const {sortColumn, sortDirection, pageSize, page, searchTerm} = this._state;

    // 1. sort
    let games = sort(this.allGames, sortColumn, sortDirection);

    // 2. filter
    games = games.filter(game => matches(game, searchTerm, this.pipe));
    const total = games.length;

    // 3. paginate
    games = games.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
    return of({games, total});
  }
  
  onSort({column, direction}: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.sortColumn = column;
    this.sortDirection = direction;
  }


}

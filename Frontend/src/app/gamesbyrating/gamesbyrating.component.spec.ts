import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GamesbyratingComponent } from './gamesbyrating.component';

describe('GamesbyratingComponent', () => {
  let component: GamesbyratingComponent;
  let fixture: ComponentFixture<GamesbyratingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GamesbyratingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GamesbyratingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule, DecimalPipe } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainComponent } from './main/main.component';
import { UserService } from './user-service/user-service';
import {HttpClientModule} from '@angular/common/http';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { HomepageComponent } from './homepage/homepage.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PlatformItemComponent } from './platform-item/platform-item.component';
import { LocalStorage } from './service/localstorage';
import { StorageServiceModule } from 'ngx-webstorage-service';
import { MygamesComponent } from './mygames/mygames.component';
import { WishlistComponent } from './wishlist/wishlist.component';
import { RecommendationComponent } from './recommendation/recommendation.component';
import { PlatformPageComponent } from './platform-page/platform-page.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AllPlatformsComponent } from './all-platforms/all-platforms.component';
import { AllGenresComponent } from './all-genres/all-genres.component';
import { GamesListComponent } from './games-list/games-list.component';
import { NgbdSortableHeader } from './service/sortable.directive';
import { GamedetailsComponent } from './gamedetails/gamedetails.component';
import { LibraryComponent } from './library/library.component';
import { GamesbyratingComponent } from './gamesbyrating/gamesbyrating.component';
import { GenreItemComponent } from './genre-item/genre-item.component';
import { GnreNavComponent } from './gnre-nav/gnre-nav.component';



@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    MainComponent,
    ConfirmationComponent,
    HomepageComponent,
    PlatformItemComponent,
    MygamesComponent,
    WishlistComponent,
    RecommendationComponent,
    PlatformPageComponent,
    NavbarComponent,
    AllPlatformsComponent,
    AllGenresComponent,
    GamesListComponent,
    NgbdSortableHeader,
    GamedetailsComponent,
    LibraryComponent,
    GamesbyratingComponent,
    GenreItemComponent,
    GnreNavComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    StorageServiceModule,
  ],
  providers: [UserService,LocalStorage,DecimalPipe],
  bootstrap: [MainComponent]
})
export class AppModule { }

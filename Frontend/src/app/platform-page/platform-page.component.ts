import { Component, OnInit } from '@angular/core';
import { PlatformService } from '../service/platformService'
import { Router, ActivatedRoute } from '@angular/router';
import { PlatformDetails } from '../models/PlatformDetails';
import { Observable, Observer } from 'rxjs';

@Component({
  selector: 'app-platform-page',
  templateUrl: './platform-page.component.html',
  styleUrls: ['./platform-page.component.css']
})
export class PlatformPageComponent implements OnInit {

  constructor(private platformService:PlatformService,  private route: ActivatedRoute, private router: Router) { 
    this.router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
  };
  }
  details:PlatformDetails;
  base64Image: any;
  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('identifier');
    var image: string;
    this.platformService.getPlatformDetails(+id).subscribe((response: PlatformDetails) =>{
      this.details = response;
      image = response.image_background;
      this.getBase64ImageFromURL(image).subscribe(base64data => {
        this.base64Image = 'data:image/jpg;base64,' + base64data;
      });
    })
  }
  getBase64ImageFromURL(url: string) {
    return Observable.create((observer: Observer<string>) => {
      let img = new Image();
      img.crossOrigin = 'Anonymous';
      img.src = url;  img.src = url;
      if (!img.complete) {
        img.onload = () => {
          observer.next(this.getBase64Image(img));
          observer.complete();
        };
        img.onerror = (err) => {
          observer.error(err);
        };
      } else {
        observer.next(this.getBase64Image(img));
        observer.complete();
      }
    });
  }

  getBase64Image(img: HTMLImageElement) {
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    var dataURL = canvas.toDataURL("image/png");
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
  }
}

import { Component, OnInit } from '@angular/core';
import { GameDetails } from '../models/GameDetails';
import { LocalStorage } from '../service/localstorage';
import { Observable, Observer } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-gamedetails',
  templateUrl: './gamedetails.component.html',
  styleUrls: ['./gamedetails.component.css']
})
export class GamedetailsComponent implements OnInit {

  constructor(private storage:LocalStorage,private route: ActivatedRoute, private router:Router) {
    this.router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
   }
  }

  gameDetails : GameDetails;
  base64Image: any;
  ngOnInit(): void {

    this.gameDetails=this.storage.getGameDetails();
    console.log(this.gameDetails);
    let id = this.route.snapshot.paramMap.get('identifier');
    var image: string;
      image = this.gameDetails.background_image;
      this.getBase64ImageFromURL(image).subscribe(base64data => {
        this.base64Image = 'data:image/jpg;base64,' + base64data;
      });
    }

  getBase64ImageFromURL(url: string) {
    return Observable.create((observer: Observer<string>) => {
      let img = new Image();
      img.crossOrigin = 'Anonymous';
      img.src = url;  img.src = url;
      if (!img.complete) {
        img.onload = () => {
          observer.next(this.getBase64Image(img));
          observer.complete();
        };
        img.onerror = (err) => {
          observer.error(err);
        };
      } else {
        observer.next(this.getBase64Image(img));
        observer.complete();
      }
    });
  }

  getBase64Image(img: HTMLImageElement) {
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    var dataURL = canvas.toDataURL("image/png");
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
  }

}

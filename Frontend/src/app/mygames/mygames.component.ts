import { Component, OnInit } from '@angular/core';
import { LocalStorage } from '../service/localstorage';

@Component({
  selector: 'app-mygames',
  templateUrl: './mygames.component.html',
  styleUrls: ['./mygames.component.css']
})
export class MygamesComponent implements OnInit {

  constructor(private storage:LocalStorage) { }

  user:String = this.storage.getUser();
  ngOnInit(): void {
  }

}

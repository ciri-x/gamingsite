import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PlatformService } from '../service/platformService'
import { Platform } from '../models/Platform';
import { UserService } from '../user-service/user-service';
import { LocalStorage } from '../service/localstorage';
import { GameService } from '../service/gameService';
import { GamebyID } from '../models/Game-byID';
import { GameRaitingDetails } from '../models/GameRaitingDetails';
import { GenreDetails } from '../models/GenreDetails';
import { Genre } from '../models/Genre';
import { GenreService } from '../service/genreService';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  platforms:Platform[];
  genres:Genre[];
  constructor(private user: LocalStorage, private platformService:PlatformService,private router:Router, private service:UserService,private gameService:GameService, private genreService:GenreService) { }
  
  korisnik:String;
  check:boolean = false;

  ngOnInit(): void {
    this.korisnik = this.user.getUser();
    console.log(this.korisnik);
    if(this.korisnik){
      this.check=true;
    }
    this.platformService.getFewPlatforms().subscribe(platforms => {
      this.platforms = platforms;
    });
    this.genreService.getFewGenres().subscribe((genres=>{
      this.genres=genres;
      console.log(this.genres);
    }));
  }
  logout(){
    this.service.logOut();
    this.router.navigate(['/homepage']).then(()=>{
      window.location.reload();
    })
  }
  getAllPlatforms(page: string){
    this.router.navigate(['/platforms', {page: page}]);
  }

  getAllGenres(page: string){
    this.router.navigate(['/genres', {page: page}]);
  }
  gamesPopis(){
    this.gameService.getAllGames().subscribe((response: GamebyID[]) =>{
      this.user.setGames(response);
      console.log(this.user.getGames());
      this.router.navigate(['/games-list'])
    });
  }

  gamesRating(){
    this.gameService.getGamesByRating().subscribe((response: GameRaitingDetails[]) =>{
      this.user.setGamesRating(response);
      console.log(this.user.getGamesRating());
      this.router.navigate(['/gamesbyrating']);
  })
  }
}


import { Injectable, Inject } from "@angular/core";
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { GameDetails } from '../models/GameDetails';
import { GenreDetails } from '../models/GenreDetails';
import { GamebyID } from '../models/Game-byID';
import { GameRaitingDetails } from '../models/GameRaitingDetails';


@Injectable()
export class LocalStorage{

    STORAGE_KEY = 'userData';
    STORAGE_ID = 'userID'
    STORAGE_DETAILS = 'gameDetails'
    GENRE_DETAILS = 'genreDetails'
    GAMES = 'gameID'
    GAMESRATING = 'gameRating'

    username: String;
    id: number;

    constructor(@Inject(LOCAL_STORAGE) private storage: StorageService){}


    public clearUser(){
        this.storage.clear()
    }

    public setUserData(korisnik:String){
        this.storage.set(this.STORAGE_KEY,korisnik);
    }

    public setUserId(idi:number){
        this.storage.set(this.STORAGE_ID,idi);
    }

    public getId(){
        let id:number=this.storage.get(this.STORAGE_ID);
        return id;
    }

    public getUser(){
        let username:String = this.storage.get(this.STORAGE_KEY);
        return username;
    }

    public setGameDetails(details:GameDetails){
        this.storage.set(this.STORAGE_DETAILS,details);

    }

    public getGameDetails(){
        return this.storage.get(this.STORAGE_DETAILS);
    }

    public setGenreDetails(details:GenreDetails){
        this.storage.set(this.GENRE_DETAILS,details);

    }

    public getGenreDetails(){
        return this.storage.get(this.GENRE_DETAILS);
    }

    public setGames(games:GamebyID[]){
        this.storage.set(this.GAMES,games);
    }
    public getGames(){
        return this.storage.get(this.GAMES);
    }

    public setGamesRating(games:GameRaitingDetails[]){
        this.storage.set(this.GAMESRATING,games);
    }

    public getGamesRating(){
        return this.storage.get(this.GAMESRATING);
    }


}
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../models/User';
import { ALLUSERS, LIBRARY, REMOVEFROMLIBRARY } from './service';
import { Wish } from '../models/Wishlist';


@Injectable({
    providedIn: 'root'
})

export class LibraryService{

    constructor(private http:HttpClient){
    }

    getAllUsers():Observable<User[]>{
        return this.http.get<User[]>(ALLUSERS);
    }

    getLibrary(id:number){
        return this.http.get<Wish[]>(LIBRARY+id)
    }

    removeFromLibrary(gameId:number,userId:number){
        return this.http.delete(REMOVEFROMLIBRARY+gameId+"&userID="+userId)

    }
}
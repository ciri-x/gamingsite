import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { GamebyID } from '../models/Game-byID';
import { ALLGAMESID, GETGAMEBYID, GAMESBYRATING } from '../service/service';
import  {Observable } from 'rxjs';
import { GameDetails } from '../models/GameDetails';
import { GameRaitingDetails } from '../models/GameRaitingDetails';


@Injectable({
    providedIn: 'root'
})
export class GameService {

    constructor(private http:HttpClient){ 
    }
    
    getAllGames():Observable<GamebyID[]>{
        return this.http.get<GamebyID[]>(ALLGAMESID);
    }

    getGameDetails(gameID:number):Observable<GameDetails>{
        return this.http.get<GameDetails>(GETGAMEBYID+gameID);
    }

    getGamesByRating():Observable<GameRaitingDetails[]>{
        return this.http.get<GameRaitingDetails[]>(GAMESBYRATING);
    }
    
    
}
import { RECOMMENDATION, ALLUSERS } from './service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/User';
import { Wish } from '../models/Wishlist';


@Injectable({
    providedIn: 'root'
})

export class RecommendationService{

    constructor(private http:HttpClient){
    }

    getAllUsers():Observable<User[]>{
        return this.http.get<User[]>(ALLUSERS);
    }

    getRecommendation(id:number){
        return this.http.get<Wish[]>(RECOMMENDATION+id)
    }

}
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/User';
import { ALLUSERS,WISH, REMOVEFROMWISHLIST } from './service';
import { Wish } from '../models/Wishlist';


@Injectable({
    providedIn: 'root'
})

export class WishlistService{

    constructor(private http:HttpClient){
    }

    getAllUsers():Observable<User[]>{
        return this.http.get<User[]>(ALLUSERS);
    }

    getWishList(id:number){
        return this.http.get<Wish[]>(WISH+id)
    }

    removeFromWishlist(gameId:number,userId:number){
        return this.http.delete(REMOVEFROMWISHLIST+gameId+"&userID="+userId)

    }

}
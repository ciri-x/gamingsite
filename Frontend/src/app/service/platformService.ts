import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { PLATFORMDETAILS, FEWPLATFORMS, ALLPLATFORMS } from '../service/service';
import { Observable } from 'rxjs';
import { Platform } from '../models/Platform';
import { PlatformDetails } from '../models/PlatformDetails';
import { AllPlatforms } from '../models/AllPlatforms';
@Injectable({
    providedIn: 'root'
})

export class PlatformService{
    constructor(private http:HttpClient){ }

    getPlatformDetails(id:number):Observable<PlatformDetails>{
        return this.http.get<PlatformDetails>(PLATFORMDETAILS + id);
    }

    getFewPlatforms():Observable<Platform[]>{
        return this.http.get<Platform[]>(FEWPLATFORMS);
    }

    getAllPlatforms(page:string):Observable<AllPlatforms>{
        return this.http.get<AllPlatforms>(ALLPLATFORMS + page);
    }
}
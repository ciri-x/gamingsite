import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { ALLGENRES, GENREDETAILS, FEWGENRES } from '../service/service';
import { Observable } from 'rxjs';
import { AllGenres } from '../models/AllGenres';
import { GenreDetails } from '../models/GenreDetails';
import { Genre } from '../models/Genre';
@Injectable({
    providedIn: 'root'
})

export class GenreService{
    constructor(private http:HttpClient){ }

    getAllGenres():Observable<AllGenres>{
        return this.http.get<AllGenres>(ALLGENRES);
    }

    getGenreDetails(id):Observable<GenreDetails>{
        return this.http.get<GenreDetails>(GENREDETAILS+id);
    }

    getFewGenres():Observable<Genre[]>{
        return this.http.get<Genre[]>(FEWGENRES);
    }
}
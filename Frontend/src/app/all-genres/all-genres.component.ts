import { Component, OnInit } from '@angular/core';
import { GenreService } from '../service/genreService'
import { Router, ActivatedRoute } from '@angular/router';
import { AllGenres } from '../models/AllGenres';
import { GenreDetails } from '../models/GenreDetails';
import { LocalStorage } from '../service/localstorage';
@Component({
  selector: 'app-all-genres',
  templateUrl: './all-genres.component.html',
  styleUrls: ['./all-genres.component.css']
})
export class AllGenresComponent implements OnInit {

  constructor(private genreService:GenreService, private route: ActivatedRoute, private router: Router,private localstorage:LocalStorage) {
    this.router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
    }
   }
  allGenres:AllGenres;
  ngOnInit(): void {
    this.genreService.getAllGenres().subscribe((response: AllGenres) =>{
      this.allGenres = response;
      console.log(this.allGenres);
    })
  }

  details(id:number){
    this.genreService.getGenreDetails(id).subscribe((response:GenreDetails)=>{
      this.localstorage.setGenreDetails(response);
      this.router.navigate(['/genreDetails']);

    })
  }

}

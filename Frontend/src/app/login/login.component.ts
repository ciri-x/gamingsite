import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UserService } from '../user-service/user-service';
import { Router } from '@angular/router';
import { JsonPipe } from '@angular/common';
import { LocalStorage } from '../service/localstorage';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private userService: UserService, private router: Router, private user:LocalStorage) { }

  stanje:number;

  loginForm = new FormGroup({
    username: new FormControl(),
    password: new FormControl()
  });
  ngOnInit() {
  }

  loginUser(loginForm:FormGroup){
    this.userService.loginUser(loginForm).subscribe((response) =>{
      console.log(response);
    },
    (errRes) =>{
      console.log(errRes.status);
      this.stanje = errRes.status;
      console.log(this.stanje);
      if(this.stanje == 200){
        this.user.setUserData(loginForm.get('username').value);
        this.router.navigate(['/homepage']);
      }
    }
    )
  }

}
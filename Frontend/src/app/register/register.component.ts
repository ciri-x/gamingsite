import { Component, OnInit } from '@angular/core';
import {FormControl,FormGroup} from '@angular/forms'
import { Router } from '@angular/router';
import { UserService } from '../user-service/user-service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
    email: new FormControl(''),
    firstName: new FormControl(''),
    lastName: new FormControl('')

  });

  constructor(private router:Router, private userService: UserService) { }


  registerUser(registerForm: FormGroup){
      this.userService.registerUser(registerForm).subscribe((response) =>{
        console.log(response);
        this.router.navigate(['/confirm']);
      })

    }
  ngOnInit() {
  }

}

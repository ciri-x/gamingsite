import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { HomepageComponent } from './homepage/homepage.component';
import { MygamesComponent } from './mygames/mygames.component';
import { WishlistComponent } from './wishlist/wishlist.component';
import { RecommendationComponent } from './recommendation/recommendation.component';
import { NavbarComponent } from './navbar/navbar.component'
import { PlatformPageComponent } from './platform-page/platform-page.component';
import { AllPlatformsComponent } from './all-platforms/all-platforms.component';
import { AllGenresComponent } from './all-genres/all-genres.component';
import { GamesListComponent } from './games-list/games-list.component';
import { GamedetailsComponent } from './gamedetails/gamedetails.component';
import { LibraryComponent } from './library/library.component';
import { GamesbyratingComponent } from './gamesbyrating/gamesbyrating.component';
import { GenreItemComponent } from './genre-item/genre-item.component';


const routes: Routes = [
  {path: 'register', component : RegisterComponent},
  {path: 'login', component : LoginComponent},
  {path: 'confirm', component:ConfirmationComponent},
  {path: 'homepage', component:HomepageComponent},
  {path: 'mygames', component:MygamesComponent},
  {path: 'wishlist', component:WishlistComponent},
  {path: 'recommendation', component:RecommendationComponent},
  {path: 'platform-page', component:PlatformPageComponent},
  {path: 'navbar', component:NavbarComponent},
  {path: 'platforms', component:AllPlatformsComponent},
  {path: 'genres', component:AllGenresComponent},
  {path : 'games-list', component:GamesListComponent},
  {path: 'gamedetails',component:GamedetailsComponent},
  {path: 'library',component:LibraryComponent},
  {path: 'gamesbyrating',component:GamesbyratingComponent},
  {path: 'genreDetails',component:GenreItemComponent},
  {path: '**', redirectTo:'homepage',pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export interface GamebyID {
    gameID : number;
    gameRating : number;
    gameTitle : string;
    id : number;
}
import { Game } from './Game';
export class Platform {
    games ?: Game[];
    games_count: number;
    id: number;
    image_background: string;
    name: string;
    slug: string;
    image ?: {};
    year_end ?: {};
    year_start ?: {};
}
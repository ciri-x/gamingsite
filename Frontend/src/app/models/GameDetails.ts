import { Developer } from './Developer';
import { Genre } from './Genre';
import { Platform } from './Platform';
import { PlatformDetails } from './PlatformDetails';
import { Publisher } from './Publisher';
import { Tag } from './Tag';

export class GameDetails {
    background_image: string;
    background_image_additional : string;
    description : string;
    developers : Developer[];
    genres : Genre[];
    id : number;
    name : string;
    parent_platforms : Platform[];
    platforms : [{
        games_count: number;
        id: number;
        image : {};
        image_background : string;
        name : string;
        platform : PlatformDetails;
        released_at : string;
        requirements : {};
        slug : string;
        year_end : {};
        year_start : {};
    }
    ];
    publishers : Publisher[];
    rating : number;
    released : string;
    slug : string;
    tags : Tag[];
    website : string;

}
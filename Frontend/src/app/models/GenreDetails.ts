export class GenreDetails {
    description: string;
    games_count: number;
    id: number
    image?: string;
    image_background: string;
    name: string;
    slug: string;
}
import { Genre } from './Genre'
export class AllGenres {
    count : number;
    description?:string;
    next ?: {};
    previous ?: {};
    results:Genre[];
    seo_description?:string;
    seo_h1?:string;
    seo_title?:string;
}
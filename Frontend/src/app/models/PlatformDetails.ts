export class PlatformDetails {
    description: string;
    games_count: number;
    id: number
    image?: string;
    image_background: string;
    name: string;
    slug: string;
    year_end?: number;
    year_start?: number;
}
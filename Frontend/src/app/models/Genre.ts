import { Platform } from './Platform';
import { Game } from './Game';
export class Genre {
    games?:Game[];
    games_count:number;
    id:number;
    image?:{};
    image_background:string;
    name:string;
    slug:string;
    year_end?:{};
    year_start?:{}
}
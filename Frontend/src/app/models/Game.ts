export class Game {
    id: number;
    slug : string;
    name : string;
    added : number;
}
export class User{
    confirmationToken: string;
    confirmed: boolean;
    email: string;
    firstName: string;
    id: number;
    lastName: string;
    library: [{
        id: number;
    }];
    password: string;
    role: string;
    username: string;
    wishlist: [{
        id: number;
    }];
}
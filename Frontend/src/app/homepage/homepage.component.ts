import { Component, OnInit } from '@angular/core';
import { WishlistService } from '../service/wishlist';
import { LocalStorage } from '../service/localstorage';
import { User } from '../models/User';
@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  constructor(private service:WishlistService,private user:LocalStorage) { }
  korisnici: User[];
  idic:number;
  korisnik = this.user.getUser();
  ngOnInit() {
    console.log(this.user.getGames());
    console.log(this.user.getGamesRating());
    this.service.getAllUsers().subscribe((response: User[]) =>{
      this.korisnici = response;
      this.korisnici.forEach(kor =>{
        if(kor.username == this.korisnik){
          this.idic=kor.id;
        }
      })
      this.user.setUserId(this.idic);
      console.log("tu sam");
    })
  }
}

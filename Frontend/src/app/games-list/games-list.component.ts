import { Component, OnInit , QueryList, ViewChildren} from '@angular/core';
import { FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { GameService } from '../service/gameService'
import { GamebyID } from '../models/Game-byID';
import { Router } from '@angular/router';
import {Injectable, PipeTransform} from '@angular/core';
import {DecimalPipe} from '@angular/common';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {debounceTime, delay, switchMap, tap} from 'rxjs/operators';
import {SortColumn, SortDirection} from '../service/sortable.directive';
import {NgbdSortableHeader, SortEvent} from '../service/sortable.directive';
import { HttpClient } from '@angular/common/http';
import { ADDTOWISHLIST, ADDTOLIBRARY } from '../service/service';
import { LocalStorage } from '../service/localstorage';
import { GameDetails } from '../models/GameDetails';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
interface SearchResult {
  games: GamebyID[];
  total: number;
}
interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: SortColumn;
  sortDirection: SortDirection;
}

const compare = (v1: string, v2: string) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
function sort(games: GamebyID[], column: SortColumn, direction: string): GamebyID[] {
  if (direction === '' || column === '') {
    return games;
  } else {
    return [...games].sort((a, b) => {
      const res = compare(`${a[column]}`, `${b[column]}`);
      return direction === 'asc' ? res : -res;
    });
  }
}
function matches(game: GamebyID, term: string, pipe: PipeTransform) {
  return game.gameTitle.toLowerCase().includes(term.toLowerCase());
}

@Component({
  selector: 'app-games-list', 
  templateUrl: './games-list.component.html',
   providers: [GameService, DecimalPipe]
})
@Injectable({providedIn: 'root'})
export class GamesListComponent {
  gameDetails: GameDetails;
  private _state: State = {
    page: 1,
    pageSize: 30,
    searchTerm: '',
    sortColumn: '',
    sortDirection: ''
  };
  private _loading$ = new BehaviorSubject<boolean>(true);
  private _search$ = new Subject<void>();
  private _games$ = new BehaviorSubject<GamebyID[]>([]);
  private _total$ = new BehaviorSubject<number>(0);
  allGames : GamebyID[];
  closeResult='';
  itemName:string;
  savedTo:string;
  check:boolean = false;
  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  constructor(private gameService:GameService, private router: Router ,private pipe: DecimalPipe, private htttp:HttpClient,private localstorage:LocalStorage,private modalService:NgbModal) {
    this.allGames=localstorage.getGames();
    if(localstorage.getUser()){
      this.check=true;
    }
    console.log(this.allGames);
    this._search$.pipe(
      tap(() => this._loading$.next(true)),
      debounceTime(200),
      switchMap(() => this._search()),
      delay(200),
      tap(() => this._loading$.next(false))
    ).subscribe(result => {
      this._games$.next(result.games);
      this._total$.next(result.total);
    });
    this._search$.next();
  }
  get games$() { return this._games$.asObservable(); }
  get total$() { return this._total$.asObservable(); }
  get loading$() { return this._loading$.asObservable(); }
  get page() { return this._state.page; }
  get pageSize() { return this._state.pageSize; }
  get searchTerm() { return this._state.searchTerm; }

  set page(page: number) { this._set({page}); }
  set pageSize(pageSize: number) { this._set({pageSize}); }
  set searchTerm(searchTerm: string) { this._set({searchTerm}); }
  set sortColumn(sortColumn: SortColumn) { this._set({sortColumn}); }
  set sortDirection(sortDirection: SortDirection) { this._set({sortDirection}); }
  private _set(patch: Partial<State>) {
    Object.assign(this._state, patch);
    this._search$.next();
  }
  private _search(): Observable<SearchResult> {
    const {sortColumn, sortDirection, pageSize, page, searchTerm} = this._state;

    // 1. sort
    let games = sort(this.allGames, sortColumn, sortDirection);

    // 2. filter
    games = games.filter(game => matches(game, searchTerm, this.pipe));
    const total = games.length;

    // 3. paginate
    games = games.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
    return of({games, total});
  }
  
  onSort({column, direction}: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.sortColumn = column;
    this.sortDirection = direction;
  }

  addToWishlist(item){
    this.itemName=item.gameTitle;
    this.savedTo='Wishlist';
    this.htttp.put(ADDTOWISHLIST+item.gameID+"&gameTitle="+item.gameTitle+"&userID="+this.localstorage.getId(),{}).subscribe((response)=>{
      console.log(response);
    })
  }

  addToLibrary(item){
    this.itemName=item.gameTitle;
    this.savedTo='Library';
    this.htttp.put(ADDTOLIBRARY+item.gameID+"&gameTitle="+item.gameTitle+"&userID="+this.localstorage.getId(),{}).subscribe((response)=>{
      console.log(response);
    })
  }

  details(item){
    this.gameService.getGameDetails(item.gameID).subscribe((response:GameDetails)=>{
      this.gameDetails=response;
      this.localstorage.setGameDetails(this.gameDetails);
      this.router.navigate(['/gamedetails'])
    })
  }
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'})
    };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}


import { Component, OnInit } from '@angular/core';
import { PlatformService } from '../service/platformService'
import { Router, ActivatedRoute } from '@angular/router';
import { AllPlatforms } from '../models/AllPlatforms';
@Component({
  selector: 'app-all-platforms',
  templateUrl: './all-platforms.component.html',
  styleUrls: ['./all-platforms.component.css']
})
export class AllPlatformsComponent implements OnInit {

  constructor(private platformService:PlatformService, private route: ActivatedRoute, private router: Router) {
    this.router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
    }
   }
  allPlatforms:AllPlatforms;
  ngOnInit(): void {
    let page = this.route.snapshot.paramMap.get('page');
    this.platformService.getAllPlatforms(page).subscribe((response: AllPlatforms) =>{
      this.allPlatforms = response;
      console.log(this.allPlatforms);
    })
  }

  details(id :number){
    this.router.navigate(['/platform-page', {identifier: id}]);
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllPlatformsComponent } from './all-platforms.component';

describe('AllPlatformsComponent', () => {
  let component: AllPlatformsComponent;
  let fixture: ComponentFixture<AllPlatformsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllPlatformsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllPlatformsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { GenreDetails } from '../models/GenreDetails';
import { LocalStorage } from '../service/localstorage';
import { Observable, Observer } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-genre-item',
  templateUrl: './genre-item.component.html',
  styleUrls: ['./genre-item.component.css']
})
export class GenreItemComponent implements OnInit {

  constructor(private storage:LocalStorage,private route: ActivatedRoute, private router:Router) {
    this.router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
   }
  }

  genreDetails : GenreDetails;
  base64Image: any;
  ngOnInit(): void {

    this.genreDetails=this.storage.getGenreDetails();
    console.log(this.genreDetails);
    var image: string;
      image = this.genreDetails.image_background;
      this.getBase64ImageFromURL(image).subscribe(base64data => {
        this.base64Image = 'data:image/jpg;base64,' + base64data;
      });
    }

  getBase64ImageFromURL(url: string) {
    return Observable.create((observer: Observer<string>) => {
      let img = new Image();
      img.crossOrigin = 'Anonymous';
      img.src = url;  img.src = url;
      if (!img.complete) {
        img.onload = () => {
          observer.next(this.getBase64Image(img));
          observer.complete();
        };
        img.onerror = (err) => {
          observer.error(err);
        };
      } else {
        observer.next(this.getBase64Image(img));
        observer.complete();
      }
    });
  }

  getBase64Image(img: HTMLImageElement) {
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    var dataURL = canvas.toDataURL("image/png");
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
  }
}


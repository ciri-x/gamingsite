import { Component, OnInit } from '@angular/core';
import { LocalStorage } from '../service/localstorage';
import { HttpClient } from '@angular/common/http';
import { ALLUSERS } from '../service/service';
import { User } from '../models/User';
import { WishlistService } from '../service/wishlist';
import { VirtualTimeScheduler } from 'rxjs';
import { Wish } from '../models/Wishlist';
import { GameService } from '../service/gameService';
import { GameDetails } from '../models/GameDetails';
import { Router } from '@angular/router';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css']
})
export class WishlistComponent implements OnInit {
  korisnik = this.user.getId()
  constructor(private user:LocalStorage, private service:WishlistService, private gameservice:GameService,private router:Router) { }
  popis :Wish[]
  gameDetails:GameDetails;
  ngOnInit(): void {
    console.log(this.korisnik);
    this.service.getWishList(this.korisnik).subscribe((res:Wish[]) =>{
      this.popis = res;
      console.log(this.popis);  

    })
    }

    remove(item){
      this.service.removeFromWishlist(item.gameID,this.korisnik).subscribe((response)=>{
        console.log(response);
        location.reload();
      })
    }
    details(item){
      this.gameservice.getGameDetails(item.gameID).subscribe((response:GameDetails)=>{
        this.gameDetails=response;
        this.user.setGameDetails(this.gameDetails);
        this.router.navigate(['/gamedetails'])
      })
    }


  }


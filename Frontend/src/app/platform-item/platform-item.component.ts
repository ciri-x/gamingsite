import { Component, OnInit, Input } from '@angular/core';
import { Platform } from '../models/Platform';
import { PlatformService } from '../service/platformService'
import { Router } from '@angular/router';
@Component({
  selector: 'app-platform-item',
  templateUrl: './platform-item.component.html',
  styleUrls: ['./platform-item.component.css']
})
export class PlatformItemComponent implements OnInit {

  @Input() platform: Platform;

  constructor(private platformService:PlatformService, private router: Router) { }

  ngOnInit(): void {
  }

  getPlatformDetails(id:number){
    this.router.navigate(['/platform-page', {identifier: id}]);
  }
}

import { Component, OnInit } from '@angular/core';
import { Wish } from '../models/Wishlist';
import { LibraryService } from '../service/libraryservice';
import { LocalStorage } from '../service/localstorage';
import { GameService } from '../service/gameService';
import { GameDetails } from '../models/GameDetails';
import { Router } from '@angular/router';

@Component({
  selector: 'app-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.css']
})
export class LibraryComponent implements OnInit {

  constructor(private localstorage:LocalStorage,private service: LibraryService, private gameservice:GameService,private router:Router) { }

  popis:Wish[];
  user = this.localstorage.getId();
  gameDetails:GameDetails;

  ngOnInit(): void {
   this.service.getLibrary(this.user).subscribe((resposne:Wish[])=>{
      this.popis=resposne;
      console.log(this.popis);
    })


  }

  details(item){
    this.gameservice.getGameDetails(item.gameID).subscribe((response:GameDetails)=>{
      this.gameDetails=response;
      this.localstorage.setGameDetails(this.gameDetails);
      this.router.navigate(['/gamedetails'])
    })
  }

  remove(item){
    this.service.removeFromLibrary(item.gameID,this.user).subscribe((response)=>{
      console.log(response);
      location.reload();
    })
  }

}

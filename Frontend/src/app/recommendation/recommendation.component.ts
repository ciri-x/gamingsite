import { Component, OnInit } from '@angular/core';
import { RecommendationService } from '../service/recommendationService';
import { LocalStorage } from '../service/localstorage';
import { GameService } from '../service/gameService';
import { Router } from '@angular/router';
import { Wish } from '../models/Wishlist';
import { GameDetails } from '../models/GameDetails';

@Component({
  selector: 'app-recommendation',
  templateUrl: './recommendation.component.html',
  styleUrls: ['./recommendation.component.css']
})
export class RecommendationComponent implements OnInit {

  korisnik = this.user.getId()
  constructor(private user:LocalStorage, private service:RecommendationService, private gameservice:GameService,private router:Router) { }
  popis :Wish[]
  gameDetails:GameDetails;
  ngOnInit(): void {
    console.log(this.korisnik);
    this.service.getRecommendation(this.korisnik).subscribe((res:Wish[]) =>{
      this.popis = res;
      console.log(this.popis);  

    })
    }
    details(item){
      this.gameservice.getGameDetails(item.gameID).subscribe((response:GameDetails)=>{
        this.gameDetails=response;
        this.user.setGameDetails(this.gameDetails);
        this.router.navigate(['/gamedetails'])
      })
    }

}
